<?php
use PHPUnit\Framework\TestCase;

class BasicTest extends TestCase
{


    public function testAdd()
    {
        $result = 3+1;
        $this->assertEquals(3, $result);
    }

 	public function testSub()
    {
        $result = 3-1;
        $this->assertEquals(2, $result);
    }

 	public function testMultiple()
    {
        $result = 3*2;
        $this->assertEquals(6, $result);
    }

}